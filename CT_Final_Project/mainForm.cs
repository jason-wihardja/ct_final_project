﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace CT_Final_Project
{
    public partial class mainForm : Form
    {
        private int currentState;
        private int counter;

        private readonly String imageFolderPath = Path.GetFullPath(System.Environment.CurrentDirectory + @"\Images\");

        public mainForm()
        {
            InitializeComponent();
        }

        public void changeImage(String fileName)
        {
            Assembly thisProgram = Assembly.GetExecutingAssembly();
            var stream = thisProgram.GetManifestResourceStream("CT_Final_Project.Images." + fileName + ".png");
            pictureBox.Image = Image.FromStream(stream);
        }

        private void init()
        {
            currentState = counter = -1;

            inputTextBox.Text = "";
            inputTextBox.Enabled = true;
            inputTextBox.Focus();

            timer.Enabled = false;

            changeImage("dfa");
        }

        private Boolean checkInput()
        {
            Boolean result = true;

            if (inputTextBox.Text.Length <= 0)
            {
                result = false;
                MessageBox.Show(this, "Please enter an input!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                init();
            }
            else
            {
                foreach (char c in inputTextBox.Text)
                {
                    if (!(c == 'a' || c == '*' || c == '/' || c == ' '))
                    {
                        result = false;
                        MessageBox.Show(this, "Invalid input!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        init();
                        break;
                    }
                }
            }

            return result;
        }

        private void nextStep()
        {
            if (counter == inputTextBox.Text.Length)
            {
                timer.Enabled = false;
                if (currentState == 4 || currentState == 5)
                {
                    MessageBox.Show(this, "Valid C++ comment!", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(this, "Not a valid C++ comment!", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                switch (currentState)
                {
                    case -1:
                        currentState = 0;
                        changeImage("q0");
                        break;

                    case 0:
                        if (inputTextBox.Text[counter] == '/')
                        {
                            currentState = 1;
                            changeImage("q0-q1");
                        }
                        else
                        {
                            currentState = 6;
                            changeImage("q0-q6");
                        }
                        break;

                    case 1:
                        if (inputTextBox.Text[counter] == '/')
                        {
                            currentState = 5;
                            changeImage("q1-q5");
                        }
                        else if (inputTextBox.Text[counter] == '*')
                        {
                            currentState = 2;
                            changeImage("q1-q2");
                        }
                        else
                        {
                            currentState = 6;
                            changeImage("q1-q6");
                        }
                        break;

                    case 2:
                        if (inputTextBox.Text[counter] == '*')
                        {
                            currentState = 3;
                            changeImage("q2-q3");
                        }
                        else
                        {
                            currentState = 2;
                            changeImage("q2-q2");
                        }
                        break;

                    case 3:
                        if (inputTextBox.Text[counter] == '/')
                        {
                            currentState = 4;
                            changeImage("q3-q4");
                        }
                        else if (inputTextBox.Text[counter] == '*')
                        {
                            currentState = 3;
                            changeImage("q3-q3");
                        }
                        else
                        {
                            currentState = 2;
                            changeImage("q3-q2");
                        }
                        break;

                    case 4:
                        currentState = 6;
                        changeImage("q4-q6");
                        break;

                    case 5:
                        currentState = 5;
                        changeImage("q5-q5");
                        break;

                    case 6:
                        currentState = 6;
                        changeImage("q6-q6");
                        break;
                }

                counter += 1;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            init();
        }

        private void runBtn_Click(object sender, EventArgs e)
        {
            inputTextBox.Enabled = false;

            if (checkInput())
            {
                timer.Enabled = true;
            }
        }

        private void resetBtn_Click(object sender, EventArgs e)
        {
            init();
        }

        private void helpBtn_Click(object sender, EventArgs e)
        {
            String content = "      CS202 - Computing Theory Final Project";
            content += "\n      -------------------------------------------\n";
            content += "\n      This DFA will check if a string is a valid C++ comment or not.";
            content += "\n      Simply enter an input then press enter or click Run button.\n";
            content += "\n      Note: For simplicity, let's just assume that a C++ comment";
            content += "\n      only contains /, *, a, and ' ' (space) characters.";

            MessageBox.Show(this, content, "Help", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }

        private void aboutBtn_Click(object sender, EventArgs e)
        {
            String content = "      CS202 - Computing Theory Final Project";
            content += "\n      -------------------------------------------\n";
            content += "\n      Created by:";
            content += "\n      Jason Wihardja";
            content += "\n      William Lie";
            content += "\n      Immanuel Leo";

            MessageBox.Show(this, content, "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void inputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                inputTextBox.Enabled = false;

                if (checkInput())
                {
                    timer.Enabled = true;
                }
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            nextStep();
        }
    }
}
